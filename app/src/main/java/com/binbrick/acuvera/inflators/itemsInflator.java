package com.binbrick.acuvera.inflators;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.binbrick.acuvera.MainActivity;
import com.binbrick.acuvera.R;
import com.binbrick.acuvera.database.SqlDatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;


public class itemsInflator {
	
	public AppCompatActivity activity;
	public Context context;
	public LayoutInflater inflator;
	ArrayList<HashMap<String, String>> itemArrayList;
	
	public itemsInflator(AppCompatActivity activity) {
		// TODO Auto-generated constructor stub
		this.activity = activity;

	}

	public void addItems(){
		
		inflator = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
		itemArrayList = MainActivity.itemsCRUD.getItems();

		for (int i = 0; i < itemArrayList.size(); i++) {

			HashMap<String, String> items = new HashMap<String, String>();
			items = itemArrayList.get(i);
			Log.d("ITEM", itemArrayList.get(i).get(SqlDatabaseHelper.ITEM_AMOUNT));
			View records = inflator.inflate(R.layout.list, null);
            final TextView no = (TextView) records.findViewById(R.id.no);
			final TextView date = (TextView) records.findViewById(R.id.date);
			final TextView desc = (TextView) records.findViewById(R.id.desc);
			final TextView amount = (TextView) records.findViewById(R.id.amount);

			no.setText(items.get("COUNT"));
			date.setText(items.get(SqlDatabaseHelper.ITEM_DATE));
			desc.setText(items.get(SqlDatabaseHelper.ITEM_DESC));
			amount.setText(items.get(SqlDatabaseHelper.ITEM_AMOUNT));
			MainActivity.itemContainer.addView(records);
			View v = new View(activity);
			v.setMinimumHeight(1);
			v.setBackgroundColor(Color.parseColor("#D3D3D3"));
			MainActivity.itemContainer.addView(v);

		}


		
	}
	


	
}
