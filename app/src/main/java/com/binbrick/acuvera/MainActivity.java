package com.binbrick.acuvera;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mindorks.paracamera.Camera;
import com.binbrick.acuvera.database.ItemsCRUD;
import com.binbrick.acuvera.database.UserCRUD;
import com.binbrick.acuvera.dialogs.addRefund;
import com.binbrick.acuvera.inflators.itemsInflator;

import java.io.File;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , View.OnClickListener {

    UserCRUD userCRUD;
    FloatingActionButton fab;
    addRefund addRefund;
    WindowManager.LayoutParams lp;
    DisplayMetrics metrics;
    public static Camera camera;
    public static File file;
    public static ItemsCRUD itemsCRUD;
    public static LinearLayout itemContainer;
    public static itemsInflator itemsInflator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        itemsCRUD = new ItemsCRUD();
        userCRUD  = new UserCRUD();
        itemsInflator = new itemsInflator(MainActivity.this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView sideEmail = (TextView) headerView.findViewById(R.id.sideEmail);
        TextView sideName = (TextView) headerView.findViewById(R.id.sideName);
        itemContainer = (LinearLayout) findViewById(R.id.itemContainer);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);


        itemsInflator.addItems();

        sideName.setText("Sample User");
        sideEmail.setText("sample@mail.com");

        camera = new Camera.Builder().resetToCorrectOrientation(true).setTakePhotoRequestCode(1).setDirectory("acuvera")
                .setName("acuvera" + System.currentTimeMillis()).setImageFormat(Camera.IMAGE_JPEG).setCompression(75)
                .setImageHeight(1000).build(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_QR){
            startActivity( new Intent(MainActivity.this , QR.class));

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.fab:

                    addRefund = new addRefund(MainActivity.this);
                    lp = new WindowManager.LayoutParams();
                    metrics = MainActivity.this.getResources().getDisplayMetrics();
                    lp.copyFrom(addRefund.getWindow().getAttributes());
                    lp.width = (int) (metrics.widthPixels * 0.90);
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    addRefund.show();
                    addRefund.getWindow().setAttributes(lp);
                    addRefund.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                    addRefund.setCancelable(false);

                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{

            if(requestCode == 1 && camera != null){
                file = new File(camera.getCameraBitmapPath());
                if(file.isFile()){

                }else{
                    Toast.makeText(MainActivity.this , "Kindly take the photo again" , Toast.LENGTH_LONG).show();
                }
            }else {
                Toast.makeText(MainActivity.this , "Kindly take the photo again" , Toast.LENGTH_LONG).show();
            }

        }catch (Exception e){
            Toast.makeText(MainActivity.this , "Kindly take the photo again" , Toast.LENGTH_LONG).show();
        }
    }
}
