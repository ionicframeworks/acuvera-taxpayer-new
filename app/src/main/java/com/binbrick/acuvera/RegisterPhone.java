package com.binbrick.acuvera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.binbrick.acuvera.asynctasks.registerUserAsync;
import com.mukesh.countrypicker.CountryPicker;
import com.rilixtech.CountryCodePicker;
import com.binbrick.acuvera.utilities.ConstantValues;
import com.binbrick.acuvera.utilities.FormFunctions;
import com.binbrick.acuvera.utilities.NetworkTools;

import java.text.SimpleDateFormat;

import static com.binbrick.acuvera.asynctasks.validateUserAsync.pin;


public class RegisterPhone extends AppCompatActivity {

    public static EditText countryC , phone;
    Button registerButton;
    NetworkTools networkTools;
    FormFunctions formFunctions;
    SimpleDateFormat simpleDateFormat;
    int dateWhere = 0;
    CountryPicker countryPicker;
    public static String fullnumber;
    public static CountryCodePicker ccp;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_phone);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Phone number");

        phone  = (EditText) findViewById(R.id.phone);
        //countryC = (EditText) findViewById(R.id.country);
        ccp = (CountryCodePicker) findViewById(R.id.country);

        registerButton = (Button) findViewById(R.id.registerButton);


        String[] PERMISSIONS = {Manifest.permission.INTERNET , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.LOCATION_HARDWARE , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }


        networkTools = new NetworkTools(RegisterPhone.this);
        formFunctions = new FormFunctions();

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (formFunctions.hasValue(phone)) {
                    fullnumber = ccp.getSelectedCountryCode() + phone.getText().toString();
                    new registerUserAsync(RegisterPhone.this).execute(RegisterPhone.fullnumber.trim(), BioData.fname.getText().toString().trim(),
                            BioData.lname.getText().toString().trim(), BioData.email.getText().toString().trim(), BioData.dob.getText().toString().trim(),
                            BioData.address.getText().toString().trim(), BioData.city.getText().toString().trim(), BioData.countryC.getText().toString().trim(),
                            Passport.passport_number.getText().toString().trim(), Passport.passport_country.getText().toString().trim(),
                            Passport.passport_expiry.getText().toString().trim(), pin);
                    startActivity( new Intent(RegisterPhone.this , ConfirmPhone.class));


               }else{
                   Toast.makeText(RegisterPhone.this , ConstantValues.FILL_IN_ALL_FIELDS_ERROR , Toast.LENGTH_LONG).show();
               }


            }
        });

        // countryC.setOnClickListener(this);




    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()){
//
//
//            case R.id.country:
//                countryPicker =
//                        new CountryPicker.Builder().with(RegisterPhone.this)
//                                .listener(new OnCountryPickerListener() {
//                                    @Override public void onSelectCountry(Country c) {
//                                        //DO something here
//                                        countryC.setText(c.getDialCode());
//
//                                    }
//                                })
//                                .build();
//
//
//                countryPicker.showDialog(getSupportFragmentManager());
//                break;
//
//
//
//        }
//    }



}
