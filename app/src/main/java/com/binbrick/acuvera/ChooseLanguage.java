package com.binbrick.acuvera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.binbrick.acuvera.asynctasks.getTokenAsync;
import com.binbrick.acuvera.database.ApiCRUD;
import com.binbrick.acuvera.database.UserCRUD;
import com.binbrick.acuvera.utilities.NetworkTools;
import com.webianks.library.scroll_choice.ScrollChoice;

import java.util.ArrayList;
import java.util.List;


public class ChooseLanguage extends AppCompatActivity implements View.OnClickListener  {


    NetworkTools networkTools;
    ScrollChoice chooseCountry;
    ActionBar actionBar;
    UserCRUD userCRUD;
    ApiCRUD apiCRUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_language);

        actionBar = getSupportActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Choose Language");

        userCRUD = new UserCRUD();
        apiCRUD = new ApiCRUD();
        networkTools = new NetworkTools(ChooseLanguage.this);

        if(networkTools.checkConnectivity()){

            if(!apiCRUD.exists()){
                new getTokenAsync(ChooseLanguage.this).execute();
            }

        }

        if(userCRUD.userExists()){
            startActivity( new Intent(ChooseLanguage.this , MainActivity.class));
        }

        String[] PERMISSIONS = {Manifest.permission.INTERNET , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.LOCATION_HARDWARE , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }

        networkTools = new NetworkTools(ChooseLanguage.this);
        chooseCountry = (ScrollChoice) findViewById(R.id.language);

        ApiCRUD apiCRUD = new ApiCRUD();
        List<String> data = new ArrayList<>();
        data.add("Swahili");
        data.add("German");
        data.add("Dutch");
        data.add("English");
        data.add("Chinese");
        data.add("French");
        data.add("Japanese");
        chooseCountry.addItems(data,0);

        if(networkTools.checkConnectivity()){
          if(!apiCRUD.exists()){
              new getTokenAsync(ChooseLanguage.this).execute();
          }

        }

        chooseCountry.setOnItemSelectedListener(new ScrollChoice.OnItemSelectedListener() {
            @Override
            public void onItemSelected(ScrollChoice scrollChoice, int position, String name) {
               startActivity( new Intent(ChooseLanguage.this, Landing.class));
            }
        });



    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){





        }
    }



}
