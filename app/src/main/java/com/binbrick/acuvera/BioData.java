package com.binbrick.acuvera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.binbrick.acuvera.utilities.ConstantValues;
import com.binbrick.acuvera.utilities.FormFunctions;
import com.binbrick.acuvera.utilities.NetworkTools;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;


public class BioData extends AppCompatActivity implements View.OnClickListener , DatePickerDialog.OnDateSetListener  {

    public static EditText email , phone , fname , lname , address , city ,countryC ,dob
            ,pin;
    Button registerButton;
    NetworkTools networkTools;
    FormFunctions formFunctions;
    SimpleDateFormat simpleDateFormat;
    int dateWhere = 0;
    CountryPicker countryPicker;
    ActionBar actionBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bio_data_layout);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Profile");

        //phone  = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email);
        fname = (EditText) findViewById(R.id.fname);
        lname = (EditText) findViewById(R.id.lname);
        address = (EditText) findViewById(R.id.address);
        city = (EditText) findViewById(R.id.city);
        countryC = (EditText) findViewById(R.id.country);
        dob = (EditText) findViewById(R.id.dob);
        pin = (EditText) findViewById(R.id.pin);
        email.setText("sivadronoh@gmail.com");
        fname.setText("visron");
        lname.setText("Kiptoo");
        address.setText("Address");
        city.setText("city");

        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        registerButton = (Button) findViewById(R.id.registerButton);


        String[] PERMISSIONS = {Manifest.permission.INTERNET , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.LOCATION_HARDWARE , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }


        networkTools = new NetworkTools(BioData.this);
        formFunctions = new FormFunctions();

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if(Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches()){
                   if (formFunctions.hasValue(email, fname, lname, city, dob, address)) {


                       startActivity( new Intent(BioData.this , Passport.class));


                   }else{
                       Toast.makeText(BioData.this , ConstantValues.FILL_IN_ALL_FIELDS_ERROR , Toast.LENGTH_LONG).show();
                   }

               }else{
                   Toast.makeText(BioData.this , "Email Seems Invalid" , Toast.LENGTH_LONG).show();

               }


            }
        });

        countryC.setOnClickListener(this);
        dob.setOnClickListener(this);




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.registration_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.login:
                startActivity( new Intent(BioData.this , Login.class));
                break;
            case R.id.contact:
                contact();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void contact(){
        Toast.makeText(BioData.this, "Contact Info on a popup", Toast.LENGTH_SHORT).show();
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.dob:
                dateWhere = 2;
                new SpinnerDatePickerDialogBuilder()
                        .context(BioData.this)
                        .callback(BioData.this)
                        .spinnerTheme(1)
                        .defaultDate(2018, 01, 01)
                        .build()
                        .show();
                break;

            case R.id.country:
                countryPicker =
                        new CountryPicker.Builder().with(BioData.this)
                                .listener(new OnCountryPickerListener() {
                                    @Override public void onSelectCountry(Country c) {
                                        //DO something here
                                        countryC.setText(c.getName());

                                    }
                                })
                                .build();


                countryPicker.showDialog(this);
                break;



        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);


            dob.setText(simpleDateFormat.format(calendar.getTime()));


    }


}
