package com.binbrick.acuvera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v4.app.FragmentActivity;

import com.binbrick.acuvera.interfaces.AcuveraApiServices;
import com.binbrick.acuvera.models.TokenRequest;
import com.binbrick.acuvera.models.TokenResponse;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.rilixtech.CountryCodePicker;
import com.binbrick.acuvera.asynctasks.getTokenAsync;
import com.binbrick.acuvera.asynctasks.validateUserAsync;
import com.binbrick.acuvera.database.ApiCRUD;
import com.binbrick.acuvera.utilities.ConstantValues;
import com.binbrick.acuvera.utilities.FormFunctions;
import com.binbrick.acuvera.utilities.NetworkTools;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Login extends AppCompatActivity implements View.OnClickListener {

    EditText  pin , country;
    EditText phone;
    Button loginButton;
    NetworkTools networkTools;
    FormFunctions formFunctions;
    ApiCRUD apiCRUD;
    ActionBar actionBar;
    CountryPicker countryPicker;
    Country countryObj;
    CountryCodePicker ccp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        ccp = findViewById(R.id.country);
        phone = (EditText) findViewById(R.id.phone);
        pin = (EditText) findViewById(R.id.pin);
        loginButton = (Button) findViewById(R.id.loginButton);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Login");

        apiCRUD = new ApiCRUD();


        String[] PERMISSIONS = {Manifest.permission.INTERNET , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.LOCATION_HARDWARE , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }


        networkTools = new NetworkTools(Login.this);
        formFunctions = new FormFunctions();

        if(networkTools.checkConnectivity()){
            if(!apiCRUD.exists()){
                new getTokenAsync(Login.this).execute();
            }

        }

        //country.setOnClickListener(this);
        loginButton.setOnClickListener(this);




        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstantValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TokenRequest tokenRequest = new TokenRequest();

        AcuveraApiServices service = retrofit.create(AcuveraApiServices.class);
        service.getTokenAccess("Basic " + Base64.encodeToString("admin-client:password".getBytes(), Base64.DEFAULT), tokenRequest);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.landing_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.contact:
                contact();
                break;
            case R.id.register:
                startActivity( new Intent(Login.this , BioData.class));
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    public void contact(){
        Toast.makeText(Login.this, "Contact Info on a popup", Toast.LENGTH_SHORT).show();
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.country:

                countryPicker =
                        new CountryPicker.Builder().with(Login.this)
                                .listener(new OnCountryPickerListener() {
                                    @Override public void onSelectCountry(Country c) {
                                        //DO something here
                                        country.setText(c.getDialCode());


                                    }
                                })
                                .build();
                countryPicker.showDialog(this);
                break;


            case R.id.loginButton:


                if(formFunctions.hasValue(phone , pin)){
                    if(networkTools.checkConnectivity()){
                        int num = Integer.parseInt(phone.getText().toString());
                        String number = String.valueOf(num).trim();
                        Log.e("number",ccp.getSelectedCountryCode().toString().trim()+number);
                        new validateUserAsync(ccp.getSelectedCountryCode().toString().trim()+""+number, pin.getText().toString().trim(),Login.this).execute();
                       // new validateUserAsync(ccp.getSelectedCountryCode().toString().trim()+""+phone.getText().toString().trim() , pin.getText().toString().trim() , Login.this).execute();

                    }else{
                        Toast.makeText(Login.this , ConstantValues.ERROR_INTERNET , Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(Login.this , ConstantValues.FILL_IN_ALL_FIELDS_ERROR , Toast.LENGTH_LONG).show();
                }

                break;
        }
    }
}
