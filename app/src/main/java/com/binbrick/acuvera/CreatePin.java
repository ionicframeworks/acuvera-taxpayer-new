package com.binbrick.acuvera;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.binbrick.acuvera.asynctasks.registerUserAsync;
import com.binbrick.acuvera.utilities.ConstantValues;
import com.binbrick.acuvera.utilities.FormFunctions;
import com.binbrick.acuvera.utilities.NetworkTools;


public class CreatePin extends AppCompatActivity  {

    NetworkTools networkTools;
    FormFunctions formFunctions;
    ActionBar actionBar;
    String pin, cpin;
    Button next;
    ImageView im;
    PinView pinView, Confirmpin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_pin);
        im = (ImageView) findViewById(R.id.IM);
        im.setImageBitmap(Passport.bitmap);
        pinView = (PinView) findViewById(R.id.create);
        Confirmpin = (PinView) findViewById(R.id.repeat);
        next = (Button) findViewById(R.id.next);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Create Pin");

        String[] PERMISSIONS = {Manifest.permission.INTERNET , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.LOCATION_HARDWARE , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }

        networkTools = new NetworkTools(CreatePin.this);
        formFunctions = new FormFunctions();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pin = pinView.getText().toString().trim();
                cpin = Confirmpin.getText().toString().trim();
                if (pin.isEmpty() || cpin.isEmpty()) {
                    Toast.makeText(CreatePin.this, ConstantValues.FILL_IN_ALL_FIELDS_ERROR, Toast.LENGTH_LONG).show();
                } else if (!pin.equals(cpin)) {
                    Toast.makeText(CreatePin.this, "entered pins don't match", Toast.LENGTH_LONG).show();
                } else {
                    startActivity(new Intent(CreatePin.this, RegisterPhone.class));
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }




}
