package com.binbrick.acuvera.dialogs;

/**
 * Created by root on 1/3/18.
 */

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.binbrick.acuvera.MainActivity;
import com.binbrick.acuvera.R;
import com.binbrick.acuvera.utilities.ConstantValues;
import com.binbrick.acuvera.utilities.FormFunctions;
import com.binbrick.acuvera.utilities.NetworkTools;
import com.binbrick.acuvera.utilities.Utilities;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Created by root on 8/1/16.
 */
public class addRefund extends Dialog implements View.OnClickListener , DatePickerDialog.OnDateSetListener {


    public Context context;
    AppCompatActivity activity;
    Utilities utilities;
    public LinearLayout addRelativeLayout, cancelRelativeLayout;
    NetworkTools networkTools;
    FormFunctions formFunctions;
    WindowManager.LayoutParams lp;
    DisplayMetrics metrics;
    EditText date,amount,desc;
    LinearLayout receipt,tags;
    SimpleDateFormat simpleDateFormat;


    public addRefund( Context context ) {

        super(context);
        this.context = context;
        activity = (AppCompatActivity) context;
        utilities = new Utilities();
        networkTools = new NetworkTools(context);
        formFunctions = new FormFunctions();
        lp = new WindowManager.LayoutParams();
        metrics = context.getResources().getDisplayMetrics();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_refund);

        date = (EditText) findViewById(R.id.date);
        amount = (EditText) findViewById(R.id.amount);
        desc = (EditText) findViewById(R.id.desc);
        tags = (LinearLayout) findViewById(R.id.tags);
        receipt = (LinearLayout) findViewById(R.id.receipt);

        addRelativeLayout = (LinearLayout) findViewById(R.id.addRelativeLayout);
        cancelRelativeLayout = (LinearLayout) findViewById(R.id.cancelRelativeLayout);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);


        addRelativeLayout.setOnClickListener(this);
        cancelRelativeLayout.setOnClickListener(this);
        date.setOnClickListener(this);
        receipt.setOnClickListener(this);
        tags.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.addRelativeLayout:

                if(formFunctions.hasValue(desc , amount , date)){
                    MainActivity.itemsCRUD.add(desc.getText().toString().trim() , amount.getText().toString().trim() , date.getText().toString().trim());
                    MainActivity.itemContainer.removeAllViews();
                    MainActivity.itemsInflator.addItems();
                    Toast.makeText(activity , "Item added" , Toast.LENGTH_LONG).show();
                    this.dismiss();
                }else{
                    Toast.makeText(activity , ConstantValues.FILL_IN_ALL_FIELDS_ERROR , Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.cancelRelativeLayout:
                this.dismiss();
                break;

            case R.id.receipt:

                try{
                    MainActivity.camera.takePicture();
                }catch (Exception e){

                }

                break;

            case R.id.tags:

                try{
                    MainActivity.camera.takePicture();
                }catch (Exception e){

                }

                break;

            case R.id.date:
                new SpinnerDatePickerDialogBuilder()
                        .context(context)
                        .callback(this)
                        .spinnerTheme(1)
                        .defaultDate(2018, 01, 01)
                        .build()
                        .show();
                break;


        }

    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        date.setText(simpleDateFormat.format(calendar.getTime()));
    }
}

