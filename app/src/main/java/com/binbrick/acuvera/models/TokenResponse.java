package com.binbrick.acuvera.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vodera on 24/09/2018.
 */
public class TokenResponse {

    @SerializedName("access_token")
    @Expose
    private String access_token;
    @SerializedName("token_type")
    @Expose
    public String token_type;
    @SerializedName("refresh_token")
    @Expose
    public String refresh_token;
    @SerializedName("expires_in")
    @Expose
    public int expires_in;
    @SerializedName("scope")
    @Expose
    public String scope;
    @SerializedName("user_firstname")
    @Expose
    public String user_firstname;

    public String getAccess_token() {
        return access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public String getScope() {
        return scope;
    }

    public String getUser_firstname() {
        return user_firstname;
    }
}
