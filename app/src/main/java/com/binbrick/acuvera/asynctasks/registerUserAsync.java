package com.binbrick.acuvera.asynctasks;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.binbrick.acuvera.ChoosePayment;
import com.mindorks.paracamera.Camera;
import com.binbrick.acuvera.MainActivity;
import com.binbrick.acuvera.Passport;
import com.binbrick.acuvera.database.ApiCRUD;
import com.binbrick.acuvera.database.UserCRUD;
import com.binbrick.acuvera.utilities.ConstantValues;
import com.binbrick.acuvera.utilities.JSONParser;
import com.binbrick.acuvera.utilities.Utilities;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 5/30/17.
 */

public class registerUserAsync extends AsyncTask<String, Integer, String> {

    int connection_timeout = 100000, socket_timeout = 100000;
    HttpParams http_params;
    static InputStream is = null;
    static JSONObject jObj = null;
    public static String json = "", KEY_ITEM = "userLogin", codes, resultCode ;
    JSONObject jsonObject = null;
    Camera camera;
    JSONArray jsonArray = null;
    JSONParser jParser = null;
    public String phoneNumber2;
    public static ProgressDialog pd;
    AppCompatActivity actionBarActivity;
    ApiCRUD apiCRUD;
    UserCRUD userCRUD;
    Utilities utilities;
    ContentBody cbFile;

    public registerUserAsync(AppCompatActivity activity) {
        // new getTokenAsync(activity).execute();

        actionBarActivity = activity;
        pd = new ProgressDialog(actionBarActivity);
        http_params = new BasicHttpParams();
        jParser = new JSONParser();
        userCRUD = new UserCRUD(activity);
        apiCRUD = new ApiCRUD();
        utilities = new Utilities(activity);


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd.setMessage("Registering...");
        pd.setCancelable(false);
        pd.show();

    }

    @Override
    protected String doInBackground(String... params) {


        while (!isCancelled()) {

            if (isCancelled()) {
                publishProgress(3);
                break;
            }

            // Making HTTP request
            try {

                HttpPost httpPOst = new HttpPost(ConstantValues.BASE_URL + "register/client?token="+apiCRUD.get("api"));
                httpPOst.addHeader("Cache-Control", "no-cache");
                HttpConnectionParams.setConnectionTimeout(http_params, connection_timeout);
                HttpConnectionParams.setSoTimeout(http_params, socket_timeout);
                DefaultHttpClient httpClient = new DefaultHttpClient(http_params);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                /*nameValuePairs.add(new BasicNameValuePair("completed", "0"));
                nameValuePairs.add(new BasicNameValuePair("phone", params[0]));
                nameValuePairs.add(new BasicNameValuePair("fname", params[1]));
                nameValuePairs.add(new BasicNameValuePair("lname", params[2]));
                nameValuePairs.add(new BasicNameValuePair("email", params[3]));
                nameValuePairs.add(new BasicNameValuePair("dob", params[4]));
                nameValuePairs.add(new BasicNameValuePair("address", params[5]));
                nameValuePairs.add(new BasicNameValuePair("city", params[6]));
                nameValuePairs.add(new BasicNameValuePair("country", params[7]));
                nameValuePairs.add(new BasicNameValuePair("passport_number", params[8]));
                nameValuePairs.add(new BasicNameValuePair("passport_country", params[9]));
                nameValuePairs.add(new BasicNameValuePair("passport_expiry", params[10]));
                nameValuePairs.add(new BasicNameValuePair("pin", params[11]));
                httpPOst.setEntity(new UrlEncodedFormEntity(nameValuePairs)); */

                MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                Bitmap bmp = Passport.bitmap;
                // Bitmap bmp = BitmapFactory.decodeFile(Passport.file.getPath());
                // Bitmap bmp = camera.getCameraBitmap();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                Passport.bitmap.compress(Bitmap.CompressFormat.JPEG.JPEG, 70, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());
                cbFile = new InputStreamBody(in, "image/jpeg", "img.jpeg");

                //entity.addPart("passport_image" , cbFile);
                entity.addPart("phone" , new StringBody(params[0] , Charset.forName("UTF-8")));
                entity.addPart("completed" , new StringBody("1" , Charset.forName("UTF-8")));
                entity.addPart("fname" , new StringBody(params[1] , Charset.forName("UTF-8")));
                entity.addPart("lname" , new StringBody(params[2] , Charset.forName("UTF-8")));
                entity.addPart("email" , new StringBody(params[3] , Charset.forName("UTF-8")));
                entity.addPart("dob" , new StringBody(params[4] , Charset.forName("UTF-8")));
                entity.addPart("address" , new StringBody(params[5] , Charset.forName("UTF-8")));
                entity.addPart("city" , new StringBody(params[6] , Charset.forName("UTF-8")));
                entity.addPart("country" , new StringBody(params[7] , Charset.forName("UTF-8")));
                entity.addPart("passport_number" , new StringBody(params[8] , Charset.forName("UTF-8")));
                entity.addPart("passport_image", cbFile);
                entity.addPart("passport_country" , new StringBody(params[9] , Charset.forName("UTF-8")));
                entity.addPart("passport_expiry" , new StringBody(params[10] , Charset.forName("UTF-8")));
                entity.addPart("pin" , new StringBody(params[11] , Charset.forName("UTF-8")));
                httpPOst.setEntity(entity);
                for (int i = 0; i < params.length; i++) {
                    Log.e("param", "value " + i + "is " + params[i]);
                }
                HttpResponse httpResponse = httpClient.execute(httpPOst);

                Log.d("COUNTRY" , params[9]);

                int code = httpResponse.getStatusLine().getStatusCode();
                codes = String.valueOf(code);
                if (code != HttpStatus.SC_OK && code == HttpStatus.SC_GATEWAY_TIMEOUT) {
                    httpClient.getConnectionManager().shutdown();
                    Log.d("ERROR_NET", "ERROR0");
                    return "Error";
                }
                Log.d("CODEs", codes);


                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    json = sb.toString();
                } catch (Exception e) {
                    Log.d("ERROR_NET1", "ERROR5");
                    return "Error";
                }

                // try parse the string to a JSON object

                try {
                    jsonObject = new JSONObject(json);
                } catch (JSONException e) {
                    Log.d("ERROR_NET2", e.getMessage());
                    return "Error";
                }



            } catch (UnsupportedEncodingException e) {
                Log.d("ERROR_NET4", "ERROR1");
                return "Error";
            } catch (ClientProtocolException e) {
                Log.d("ERROR_NET5", "ERROR2");
                return "Error";
            } catch (IOException e) {
                Log.d("ERROR_NET6", e.getMessage());
                return "Error";
            } catch (Exception e) {
                Log.d("ERROR_NET7", e.getMessage());
                return "Error";
            }


            if (isCancelled()) {

                publishProgress(3);
                break;
            }

            return codes;
        }
        return null;

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        pd.dismiss();

        Log.d("JSON" , json);
//        Log.d("FILE" , Passport.file.getAbsolutePath());
        Log.d("Link" , ConstantValues.BASE_URL + "register/client?token="+apiCRUD.get("api"));
        if(jsonObject != null){
            try {
                if(jsonObject.getInt("status") == 200){
                    UserCRUD userCRUD = new UserCRUD();
                    userCRUD.add("" , jsonObject.getJSONObject("data").getString("fname")+" "+jsonObject.getJSONObject("data").getString("mname") , jsonObject.getJSONObject("data").getString("email"));
                    Toast.makeText(actionBarActivity , json , Toast.LENGTH_LONG).show();
                    actionBarActivity.startActivity(new Intent(actionBarActivity, ChoosePayment.class));
                }else{
                    Log.e("response", "server says" + json);
                    Toast.makeText(actionBarActivity , json , Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Registration Error");
                    intent.putExtra(Intent.EXTRA_TEXT, "Error is " + json);
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"hdronoh@gmail.com"});
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                    actionBarActivity.startActivity(intent);
                }

            } catch (JSONException e) {
                Toast.makeText(actionBarActivity , "An error occured!!" , Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(actionBarActivity , "Error retrieving Token" , Toast.LENGTH_LONG).show();
        }





    }


}
