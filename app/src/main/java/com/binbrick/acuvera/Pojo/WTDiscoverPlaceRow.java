package com.binbrick.acuvera.Pojo;

/**
 * Created by krupal.agravat on 2017-09-06.
 */

public class WTDiscoverPlaceRow {

    String ItemName_head;
    String ItemName_footer;

    public Integer getImg() {
        return Img;
    }

    public WTDiscoverPlaceRow(Integer img, String itemName_head, String itemName_footer) {
        Img = img;
        ItemName_head = itemName_head;
        ItemName_footer = itemName_footer;
    }

    public void setImg(Integer img) {
        Img = img;
    }

    Integer Img;

    public WTDiscoverPlaceRow(String itemName_head, String itemName_footer) {
        ItemName_head = itemName_head;
        ItemName_footer = itemName_footer;
    }

    public String getItemName_head() {
        return ItemName_head;
    }

    public void setItemName_head(String itemName_head) {
        ItemName_head = itemName_head;
    }

    public String getItemName_footer() {
        return ItemName_footer;
    }

    public void setItemName_footer(String itemName_footer) {
        ItemName_footer = itemName_footer;
    }
}
