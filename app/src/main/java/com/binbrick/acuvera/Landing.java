package com.binbrick.acuvera;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.binbrick.acuvera.Adapter.WTDiscoverPlaceAdapter;
import com.binbrick.acuvera.Pojo.WTDiscoverPlaceRow;
import com.binbrick.acuvera.asynctasks.getTokenAsync;
import com.binbrick.acuvera.database.ApiCRUD;
import com.binbrick.acuvera.database.UserCRUD;
import com.binbrick.acuvera.utilities.NetworkTools;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class Landing extends AppCompatActivity {

    LinearLayout register , login;
    UserCRUD userCRUD;
    ApiCRUD apiCRUD;
    NetworkTools networkTools;


    private ViewPager vp_slider;
    private LinearLayout ll_dots;
    private TextView[] dots;
    int page_position = 0;
    android.support.v7.app.ActionBar actionBar;

    WTDiscoverPlaceAdapter sliderPagerAdapter;
    ArrayList<WTDiscoverPlaceRow> slider_image_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        actionBar = getSupportActionBar();
        actionBar.setTitle("");

        register = (LinearLayout) findViewById(R.id.register);
        login = (LinearLayout) findViewById(R.id.login);

        userCRUD = new UserCRUD();
        apiCRUD = new ApiCRUD();
        networkTools = new NetworkTools(Landing.this);

        if(networkTools.checkConnectivity()){

            if(!apiCRUD.exists()){
                new getTokenAsync(Landing.this).execute();
            }

        }

        if(userCRUD.userExists()){
            startActivity( new Intent(Landing.this , MainActivity.class));
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity( new Intent(Landing.this , BioData.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity( new Intent(Landing.this , Login.class));
            }
        });


        // For Transperant Background
        // set flags
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            this.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        else
        {
            this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // method for initialisation
        init();

        // method for adding indicators
        addBottomDots(0);

        final Handler handler = new Handler();

        //method to set next position to view pager
        final Runnable update = new Runnable() {
            public void run() {

                // 3 is total no. of pages, you can change it as per your no. of pages, index start from zero
                if (page_position == 5)
                {
                    page_position = 0;
                }
                else
                {
                    page_position = page_position + 1;
                }
                vp_slider.setCurrentItem(page_position, true);
            }
        };

        /*
         * delay = 200,
         * long period = 4000
         * you can change this as per your requirement*/
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 200, 4000);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.landing_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.contact:
                contact();
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void contact(){
        Toast.makeText(Landing.this, "Contact Info on a popup", Toast.LENGTH_SHORT).show();
    }

    private void init()
    {
        // setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        // getSupportActionBar().hide();
        vp_slider = (ViewPager) findViewById(R.id.vp_slider);
        ll_dots = (LinearLayout) findViewById(R.id.ll_dots);

        slider_image_list = new ArrayList<WTDiscoverPlaceRow>();


        // Add few items to dataList ,this should contain url of images or text which should be displayed in slider
        // here i am adding few sample image links and Strings, you can add your own

        ArrayList<WTDiscoverPlaceRow> dataList = new ArrayList<WTDiscoverPlaceRow>();
        dataList.add(new WTDiscoverPlaceRow("Add Items", "Add purchased items details including the price and name"));
        dataList.add(new WTDiscoverPlaceRow("Scan Items", "Scan all the purchased items tags and receipts"));
        dataList.add(new WTDiscoverPlaceRow("Request Packaging", "Our trusted agent will approve and package your items"));
        dataList.add(new WTDiscoverPlaceRow("Export Packages", "Export the packages to your home country"));
        dataList.add(new WTDiscoverPlaceRow("Scan Packages", "Scan packaged items to ensure they were not tampered with"));
        dataList.add(new WTDiscoverPlaceRow("Await Refund", "You will receive your refund within 48 Hours"));

        sliderPagerAdapter = new WTDiscoverPlaceAdapter(Landing.this, dataList);

        vp_slider.setAdapter(sliderPagerAdapter);

        vp_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            private static final float thresholdOffset = 0.5f;
            private boolean scrollStarted, checkDirection;

            //it will get current page position then move to next position
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
                if (checkDirection) {
                    if (thresholdOffset > positionOffset) {

                        //3 is total no of pages, you can change it as per your total no. of pages, index start from zero
                        //for left to right
                        if(position == 5)
                            page_position = 0;
                        else
                            page_position = position+1;

                        addBottomDots(page_position);
                    } else {

                        //for right to left
                        page_position = position;
                        addBottomDots(page_position);
                    }
                    checkDirection = false;
                }
            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (!scrollStarted && state == ViewPager.SCROLL_STATE_DRAGGING) {
                    scrollStarted = true;
                    checkDirection = true;
                } else {
                    scrollStarted = false;
                }
            }
        });
    }

    private void addBottomDots(int currentPage)
    {
        //4 is total no of dots, you can change it as per your total no. of pages
        dots = new TextView[6];

        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++)
        {
            dots[i] = new TextView(this);

            /*
             * here "&#8226;" is a code to draw dot, you cant set your own
             * you can replace 30 with your own textSize */
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(25);
            dots[i].setPadding(5,0,5,0);
            dots[i].setTextColor(Color.parseColor("#b7b7b7"));
            ll_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#0455A0"));
            dots[currentPage].setTextSize(50);
    }

}
