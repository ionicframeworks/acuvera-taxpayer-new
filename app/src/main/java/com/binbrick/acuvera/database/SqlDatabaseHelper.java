package com.binbrick.acuvera.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by root on 4/7/17.
 */

public class SqlDatabaseHelper extends SQLiteOpenHelper {

    public static String DB_NAME = "acuvera.db";
    public static SqlDatabaseHelper sInstance;
    public static int DB_VER = 1;


    public static String TBL_USERS = "tbl_users" , TBL_IMGS = "tbl_imgs" , TBL_ITEMS = "tbl_items" , TBL_API = "tbl_api";

    public static String USER_ID = "user_id",
            USER_PHONE = "user_phone" , USER_NAME = "user_name" , USER_EMAIL = "user_email";
    public static String ITEM_DESC = "item_desc" , ITEM_AMOUNT = "item_amount" , ITEM_DATE = "item_date";

    public static String  API_KEY = "api_key";

    public static String IMG_TYPE = "type",
            IMG_CUSTOMERID = "customerid" , IMG_LOCATION = "imgpath" , IMG_SESSION = "imgSession" , IMG_STATUS = "imgStatus";


    public static String CREATE_TBL_USERS = "CREATE TABLE " + TBL_USERS
            + " (app_user_id INTEGER PRIMARY KEY AUTOINCREMENT , " + USER_NAME + " TEXT ," + USER_PHONE + " TEXT , " + USER_EMAIL + " TEXT , " + USER_ID + " TEXT )";

    public static String CREATE_TBL_IMGS = "CREATE TABLE " + TBL_IMGS
            + " (app_location_id INTEGER PRIMARY KEY AUTOINCREMENT , " + IMG_TYPE + " TEXT , " + IMG_CUSTOMERID + " TEXT  , " + IMG_LOCATION + " TEXT , " + IMG_SESSION + " TEXT  , " + IMG_STATUS + " TEXT )";

    public static String CREATE_TBL_API = "CREATE TABLE " + TBL_API + " ( app_api INTEGER PRIMARY KEY AUTOINCREMENT , " +API_KEY+" TEXT)";

    public static String CREATE_TBL_ITEMS = "CREATE TABLE " + TBL_ITEMS + " ( app_items INTEGER PRIMARY KEY AUTOINCREMENT , " +ITEM_DESC+" TEXT , " +ITEM_AMOUNT+ " TEXT , " +ITEM_DATE+ " TEXT)";


    public SqlDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    public static synchronized SqlDatabaseHelper getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new SqlDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        try {

            sqLiteDatabase.execSQL(CREATE_TBL_USERS);
            sqLiteDatabase.execSQL(CREATE_TBL_IMGS);
            sqLiteDatabase.execSQL(CREATE_TBL_API);
            sqLiteDatabase.execSQL(CREATE_TBL_ITEMS);

        } catch (SQLException e) {
            Log.d("SQL_ERROR_ONCREATE", e.getMessage().toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {


        if (newVersion > oldVersion) {


        }

    }
}
