package com.binbrick.acuvera.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import static com.binbrick.acuvera.Acuvera.database;


/**
 * Created by root on 4/19/17.
 */

public class ItemsCRUD {

    public Activity activity;
    Cursor numRows;
    Cursor cursor;
    int rows = 0;





    public ItemsCRUD() {
    }

    public void add( String desc , String amount ,String date ) {

        ContentValues values = new ContentValues();
        values.put(SqlDatabaseHelper.ITEM_DESC, desc);
        values.put(SqlDatabaseHelper.ITEM_AMOUNT, amount);
        values.put(SqlDatabaseHelper.ITEM_DATE, date);


        try {

            database.insert(SqlDatabaseHelper.TBL_ITEMS, null, values);

        } catch (SQLException e) {

        }

    }





    public String getItem(String field) {

        try {

            switch (field) {

                case "desc":
                    cursor = database.rawQuery("SELECT " + SqlDatabaseHelper.ITEM_DESC + " FROM " + SqlDatabaseHelper.TBL_ITEMS,
                            null);
                    if (cursor.moveToFirst()) {
                        do {
                            return cursor.getString(0).toString().trim();

                        } while (cursor.moveToNext());
                    }
                    break;
                case "amount":
                    cursor = database.rawQuery("SELECT " + SqlDatabaseHelper.ITEM_AMOUNT + " FROM " + SqlDatabaseHelper.TBL_ITEMS,
                            null);
                    if (cursor.moveToFirst()) {
                        do {
                            return cursor.getString(0).toString().trim();

                        } while (cursor.moveToNext());
                    }
                    break;
                case "date":
                    cursor = database.rawQuery("SELECT " + SqlDatabaseHelper.ITEM_DATE + " FROM " + SqlDatabaseHelper.TBL_ITEMS,
                            null);
                    if (cursor.moveToFirst()) {
                        do {
                            return cursor.getString(0).toString().trim();

                        } while (cursor.moveToNext());
                    }
                    break;


            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return "";
    }



    public boolean itemExists() {

        numRows = null;
        try {
            numRows = database.rawQuery("SELECT * FROM " + SqlDatabaseHelper.TBL_ITEMS, null);
            numRows.moveToFirst();
            rows = numRows.getCount();
            if (rows == 0) {
                numRows.close();
                return false;
            } else {
                numRows.close();
                return true;
            }
        } catch (SQLException e) {
            Log.i("SQL_ERROR", e.getMessage());
        }
        return false;

    }

    public void delete(){

        try {

            database.delete(SqlDatabaseHelper.TBL_ITEMS , null , null);
        } catch (SQLException e) {

        }
    }

    public ArrayList<HashMap<String, String>> getItems(){

        ArrayList<HashMap<String, String>> itemArrayList =  new ArrayList<HashMap<String, String>>();

        int count = 1;
        cursor = database.rawQuery("SELECT " + SqlDatabaseHelper.ITEM_DESC + " , " + SqlDatabaseHelper.ITEM_AMOUNT + " , "+SqlDatabaseHelper.ITEM_DATE+" FROM " + SqlDatabaseHelper.TBL_ITEMS + " ORDER BY app_items DESC",
                null);

            if (cursor.moveToFirst()) {
                do {


                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(SqlDatabaseHelper.ITEM_DATE, cursor.getString(0).toString().trim());
                    map.put(SqlDatabaseHelper.ITEM_AMOUNT, cursor.getString(1).toString().trim());
                    map.put(SqlDatabaseHelper.ITEM_DESC, cursor.getString(2).toString().trim());
                    map.put("COUNT", Integer.toString(count));
                    count++;
                    itemArrayList.add(map);

                } while (cursor.moveToNext());
            }




        return itemArrayList;
    }



}
