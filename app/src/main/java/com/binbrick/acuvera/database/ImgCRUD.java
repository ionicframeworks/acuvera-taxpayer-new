package com.binbrick.acuvera.database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.binbrick.acuvera.utilities.ConstantValues;

import static com.binbrick.acuvera.Acuvera.database;


/**
 * Created by root on 4/19/17.
 */

public class ImgCRUD {

    public Activity activity;
    Cursor numRows;
    public static  Cursor nuRows_;
    Cursor cursor;
    int rows = 0;
    public static int rows_ = 0;
    public static String BOARD_IMG = "1" , BUILDING_IMG = "2" , INSIDE_IMG = "3" , IMG_CAPTURED = "1" , IMG_UPLOADED = "2";




    public ImgCRUD(AppCompatActivity activity) {
        this.activity = activity;
    }

    public ImgCRUD() {
    }

    public void add( String type , String location , String sesssion , String status) {



        ContentValues values = new ContentValues();
        values.put(SqlDatabaseHelper.IMG_TYPE, type);
        values.put(SqlDatabaseHelper.IMG_LOCATION, location);
        values.put(SqlDatabaseHelper.IMG_SESSION, sesssion);
        values.put(SqlDatabaseHelper.IMG_STATUS, status);

        try {

            database.insert(SqlDatabaseHelper.TBL_IMGS, null, values);
            Toast.makeText(activity.getApplicationContext() , ConstantValues.PHOTO_SAVED , Toast.LENGTH_LONG).show();
        } catch (SQLException e) {
            Toast.makeText(activity.getApplicationContext(), ConstantValues.SQL_ERROR, Toast.LENGTH_LONG).show();
        }


    }

    public void updateCustomerID( String customerID ) {

        try {

           // database.execSQL("UPDATE " + SqlDatabaseHelper.TBL_IMGS + " SET " + SqlDatabaseHelper.IMG_CUSTOMERID + " = '" + customerID.trim() + "' WHERE "+ SqlDatabaseHelper.IMG_SESSION +" ='"+ AddCustomer.imgSession.trim()+"'");

        } catch (SQLException e) {
            Log.d("SQL_ERROR", e.getMessage());
        }


    }



    public String getImg(String field , String imgSession , String status ) {


        return "";
    }


    public void updateImageStatus(String imgID , String status ){

    }



    public void delete(){

    }


    public int imgCount( String sessionID , String status ) {


        return 0;

    }

    public int totalImg(String sessionID){

        return  0;
    }


    public static int totalImgs(String sessionID){

        return  0;
    }


    public int count(String sessionID , String type){

        return  0;
    }


    public static int countStatic(String sessionID , String type){

        return  0;
    }
}
