package com.binbrick.acuvera.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import android.widget.Toast;

import com.binbrick.acuvera.utilities.ConstantValues;
import static com.binbrick.acuvera.Acuvera.database;


/**
 * Created by root on 4/19/17.
 */

public class ApiCRUD {

    public Activity activity;
    public Context context;
    Cursor numRows;
    Cursor cursor;
    int rows = 0;



    public ApiCRUD(Context context) {
        this.context = context;
    }

    public ApiCRUD() {
    }

    public void add(String apiKey) {

        this.delete();
        ContentValues values = new ContentValues();
        values.put(SqlDatabaseHelper.API_KEY, apiKey);

        try {

            database.insert(SqlDatabaseHelper.TBL_API, null, values);

        } catch (SQLException e) {
            Toast.makeText(context.getApplicationContext(), ConstantValues.SQL_ERROR, Toast.LENGTH_LONG).show();
        }


    }


    public String get(String field) {

        try {

            switch (field) {

                case "api":
                    cursor = database.rawQuery("SELECT " + SqlDatabaseHelper.API_KEY + " FROM " + SqlDatabaseHelper.TBL_API,
                            null);
                    if (cursor.moveToFirst()) {
                        do {
                            return cursor.getString(0).toString().trim();

                        } while (cursor.moveToNext());
                    }
                    break;

            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return "";
    }


    public boolean exists() {

        numRows = null;
        try {
            numRows = database.rawQuery("SELECT * FROM " + SqlDatabaseHelper.TBL_API, null);
            numRows.moveToFirst();
            rows = numRows.getCount();
            if (rows == 0) {
                numRows.close();
                return false;
            } else {
                numRows.close();
                return true;
            }
        } catch (SQLException e) {
            Log.i("SQL_ERROR", e.getMessage());
        }
        return false;

    }

    public void delete() {

        try {

            database.delete(SqlDatabaseHelper.TBL_API, null, null);

        } catch (SQLException e) {
            Toast.makeText(context.getApplicationContext(), ConstantValues.SQL_ERROR, Toast.LENGTH_LONG).show();
        }
    }


}
