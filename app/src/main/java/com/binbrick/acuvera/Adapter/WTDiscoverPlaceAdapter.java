package com.binbrick.acuvera.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.binbrick.acuvera.R;
import com.binbrick.acuvera.Pojo.WTDiscoverPlaceRow;

import java.util.ArrayList;

/**
 * Created by krupal.agravat on 2017-09-06.
 */

public class WTDiscoverPlaceAdapter extends PagerAdapter {
    Activity activity;
    ArrayList<WTDiscoverPlaceRow> image_arraylist;
    private LayoutInflater layoutInflater;

    public WTDiscoverPlaceAdapter(Activity activity, ArrayList<WTDiscoverPlaceRow> image_arraylist) {
        this.activity = activity;
        this.image_arraylist = image_arraylist;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.row_layout_landing, container, false);

        TextView tv_txth = view.findViewById(R.id.tv_txt_head);
        TextView tv_txtf = view.findViewById(R.id.tv_txt_footer);

        WTDiscoverPlaceRow dItem = this.image_arraylist.get(position);


        tv_txth.setText(dItem.getItemName_head());
        tv_txtf.setText(dItem.getItemName_footer());

        tv_txth.setTextColor(activity.getResources().getColor(R.color.blue_p20));
        tv_txtf.setTextColor(activity.getResources().getColor(R.color.wallet_holo_blue_light));

        //you can replace your own typeface over here
        Typeface headFont = Typeface.createFromAsset(activity.getAssets(), "fonts/SFUIDisplay-Bold.ttf");
        Typeface fFont = Typeface.createFromAsset(activity.getAssets(), "fonts/SF-UI-Text-Regular.ttf");
        tv_txth.setTypeface(headFont);
        tv_txtf.setTypeface(fFont);

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return image_arraylist.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
