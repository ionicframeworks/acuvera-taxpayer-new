package com.binbrick.acuvera;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.binbrick.acuvera.database.DbFunctions;

public class Acuvera extends Application {

    public static DbFunctions dbFunctions;
    public static SQLiteDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();

        dbFunctions = DbFunctions.getInstance(getApplicationContext());
        database = dbFunctions.open();


    }
}
