package com.binbrick.acuvera.interfaces;

import com.binbrick.acuvera.models.TokenRequest;
import com.binbrick.acuvera.models.TokenResponse;

import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created by vodera on 24/09/2018.
 */
public interface AcuveraApiServices {


    @POST("auth/client")
    Call<TokenResponse> getTokenAccess(
            @Header("Authorization") String auth,
            @Body TokenRequest tokenRequest);

//    @POST("users")
//    Call<Register> registerClient(
//            @Body Register register);

//    @POST("register/client")
//    Call<Register> registerUser(@Body Register register);

}
