package com.binbrick.acuvera.interfaces;

/**
 * Created by vodera on 24/09/2018.
 */

import android.util.Base64;

public class AuthorizationInterceptor {

    /**
     * this method is API implemetation specific
     * might not work with other APIs
     **/
    public static String getAuthorizationHeader(String email, String password) {
        String credential = email + ":" + password;
        return "Basic " + Base64.encodeToString(credential.getBytes(), Base64.DEFAULT);
    }
}