package com.binbrick.acuvera;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.mindorks.paracamera.Camera;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.binbrick.acuvera.utilities.ConstantValues;
import com.binbrick.acuvera.utilities.FormFunctions;
import com.binbrick.acuvera.utilities.NetworkTools;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;


public class Passport extends AppCompatActivity implements View.OnClickListener , DatePickerDialog.OnDateSetListener  {

    public static EditText passport_number, passport_country, passport_expiry
            ;
    Button registerButton;
    public static final int RequestPermissionCode = 1;
    public static Bitmap bitmap;
    Intent intent;
    Context context = Passport.this;

    NetworkTools networkTools;
    FormFunctions formFunctions;
    SimpleDateFormat simpleDateFormat;
    int dateWhere = 0;
    CountryPicker countryPicker;
    ImageView photo;
    int photoTaken = 0;
    Camera camera;
    public static File file;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passport_layout);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Passport");

        passport_number = (EditText) findViewById(R.id.passport_number);
        passport_country = (EditText) findViewById(R.id.passport_country);
        passport_expiry = (EditText) findViewById(R.id.passport_expiry);
        photo = (ImageView) findViewById(R.id.photo);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        registerButton = (Button) findViewById(R.id.registerButton);


        String[] PERMISSIONS = {Manifest.permission.INTERNET , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.LOCATION_HARDWARE , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
        }


        networkTools = new NetworkTools(Passport.this);
        formFunctions = new FormFunctions();

        camera = new Camera.Builder().resetToCorrectOrientation(true).setTakePhotoRequestCode(1).setDirectory("acuvera")
                .setName("acuvera" + System.currentTimeMillis()).setImageFormat(Camera.IMAGE_JPEG).setCompression(75)
                .setImageHeight(1000).build(this);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               if(formFunctions.hasValue( passport_number , passport_country , passport_expiry )){
                   if(networkTools.checkConnectivity()){

//                       new registerUserAsync( Passport.this).execute( BioData.fname.getText().toString().trim(),
//                               BioData.lname.getText().toString().trim() , BioData.email.getText().toString().trim() , BioData.dob.getText().toString().trim() ,
//                               BioData.address.getText().toString().trim() , BioData.city.getText().toString().trim() ,
//                               passport_number.getText().toString().trim() , passport_country.getText().toString().trim() ,
//                               passport_expiry.getText().toString().trim() , BioData.pin.getText().toString().trim());
                       startActivity(new Intent(Passport.this, CreatePin.class));
                   }else{
                       Toast.makeText(Passport.this , ConstantValues.ERROR_INTERNET , Toast.LENGTH_LONG).show();
                   }
               }else{
                   Toast.makeText(Passport.this , ConstantValues.FILL_IN_ALL_FIELDS_ERROR , Toast.LENGTH_LONG).show();
               }


            }
        });


        passport_country.setOnClickListener(this);
        passport_expiry.setOnClickListener(this);
        photo.setOnClickListener(this);




    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        try{
//
//            if(requestCode == 1 && camera != null){
//                file = new File(camera.getCameraBitmapPath());
//                if(file.isFile()){
//
//                }else{
//                    Toast.makeText(Passport.this , "Kindly take the photo again" , Toast.LENGTH_LONG).show();
//                }
//            }else {
//                Toast.makeText(Passport.this, "Kindly take the photo again,sorry for the inconvinience", Toast.LENGTH_LONG).show();
//            }
//
//        }catch (Exception e){
//            e.printStackTrace();
//            Log.e("pic_error", "err " + e.toString());
//            Toast.makeText(Passport.this, "Kindly take  photo again", Toast.LENGTH_LONG).show();
//        }
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.photo:

                try{
                    //camera.takePicture();
//                    camera.getCameraBitmap();
                    takePic();

                }catch (Exception e){
                    e.printStackTrace();
                }

                break;


            case R.id.passport_country:
                countryPicker =
                        new CountryPicker.Builder().with(Passport.this)
                                .listener(new OnCountryPickerListener() {
                                    @Override public void onSelectCountry(Country c) {
                                        //DO something here
                                        passport_country.setText(c.getName());
                                    }
                                })
                                .build();


                countryPicker.showDialog(this);

                break;

            case R.id.passport_expiry:
                dateWhere = 1;
                new SpinnerDatePickerDialogBuilder()
                        .context(Passport.this)
                        .callback(Passport.this)
                        .spinnerTheme(1)
                        .defaultDate(2018, 01, 01)
                        .build()
                        .show();
                break;

        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
            passport_expiry.setText(simpleDateFormat.format(calendar.getTime()));


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 7 && resultCode == RESULT_OK) {

            bitmap = (Bitmap) data.getExtras().get("data");

            photo.setImageBitmap(bitmap);
            registerButton.setText("Proceed");
            photoTaken = 1;
            registerButton.setBackgroundColor(getResources().getColor(R.color.sign_upbtnssix));
            registerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(context, RegisterPhone.class);
                    startActivity(in);
                }
            });
        }
    }

    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                Manifest.permission.CAMERA)) {
            // takePic();
            // Toast.makeText(ImageClass.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions((Activity) context, new String[]{
                    Manifest.permission.CAMERA}, RequestPermissionCode);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    // Toast.makeText(context,"Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
                } else {

                    Toast.makeText(context, "Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

    public void takePic() {
        //   EnableRuntimePermission();
        intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        startActivityForResult(intent, 7);
    }
}
