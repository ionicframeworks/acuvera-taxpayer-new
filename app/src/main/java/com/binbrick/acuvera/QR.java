package com.binbrick.acuvera;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import net.glxn.qrgen.android.QRCode;

public class QR extends AppCompatActivity {

    ImageView qr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        Bitmap myBitmap = QRCode.from("www.example.org").bitmap();
        ImageView myImage = (ImageView) findViewById(R.id.qr);
        myImage.setImageBitmap(myBitmap);
    }
}
